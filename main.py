import inline as inline
import matplotlib
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.feature_selection import SelectKBest, chi2, RFE
from sklearn.ensemble import RandomForestClassifier
import xgboost as xgb
import pandas as pd
import numpy as np
import math
from sklearn.metrics import mean_absolute_error, r2_score, median_absolute_error
from sklearn.model_selection import train_test_split, GridSearchCV, RandomizedSearchCV
from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import LabelEncoder
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')

#Вычисляет среднюю абсолютную процентную ошибку
def mean_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

#Вычисляет медианную абсолютную процентную ошибку
def median_absolute_percentage_error(y_true, y_pred):
    y_true, y_pred = np.array(y_true), np.array(y_pred)
    return np.median(np.abs((y_true - y_pred) / y_true)) * 100

#Печатает рассчитанные значения коэффициента детерминации, средней и медианной абсолютных ошибок
def print_metrics(prediction, val_y):
    val_mae = mean_absolute_error(val_y, prediction)
    median_AE = median_absolute_error(val_y, prediction)
    r2 = r2_score(val_y, prediction)
    print('')
    print('R\u00b2: {:.2}'.format(r2))
    print('')
    print('Средняя абсолютная ошибка: {:.3} %'.format(mean_absolute_percentage_error(val_y, prediction)))
    print('Медианная абсолютная ошибка: {:.3} %'.format(median_absolute_percentage_error(val_y, prediction)))
#процент nulloвых строк в каждом признаке
def print_null_val(df):
    for col in df.columns:
        pct_missing = np.mean(df[col].isnull())
        print('{} - {}%'.format(col, round(pct_missing * 100)))

def print_info_dataset(df):
    print('Размер Dataframe:', df.shape)
    print('\n\nИнформация о Dataframe df.info():')
    print(df.info())
#Переводим дату в дни от Рождества Христова
def date_to_days(dates):
    Days = []
    for date in dates: #16/12/2017 1/04/2017
        if (len(date) == 10):
            days = date[0:2]
            months = date[3:5]
            years = date[6:10]
            # для упрощения в месяце 30 дней, в году 365 дней
            totalDays = int(days) + int(months) * 30 + int(years) * 365
        else:
            days = date[0:1]
            months = date[2:4]
            years = date[5:9]
            # для упрощения: в месяце 30 дней, в году 365 дней
            totalDays = int(days) + int(months) * 30 + int(years) * 365
        Days.append(totalDays)
    return Days
#Функция показывает сколько уникальных элементов в каждом признаке
def unique_signs(df):
    unique_counts = pd.DataFrame.from_records([(col, df[col].nunique()) for col in df.columns],
                                              columns=['Column_Name', 'Num_Unique']).sort_values(by=['Num_Unique'])
    print(unique_counts)



df = pd.read_csv("data/housing_FULL.csv")
#df['bathroom'].fillna(df['bathroom'].mean(), inplace = True)
df['Days'] = date_to_days(df['Date'])
print_info_dataset(df)
print_null_val(df)
first_quartile = df.quantile(q=0.25)
third_quartile = df.quantile(q=0.75)
IQR = third_quartile - first_quartile
outliers = df[(df > (third_quartile + 1.5 * IQR)) | (df < (first_quartile - 1.5 * IQR))].count(axis=1)
outliers.sort_values(axis=0, ascending=False, inplace=True)
# Удаляем из датафрейма 3000 строк, подходящих под критерии выбросов
outliers = outliers.head(3000)
df.drop(outliers.index, inplace=True)
df['Price'].fillna(df['Price'].mean(), inplace = True)

#identify all categorical variables
cat_columns = df.select_dtypes(['object']).columns

#convert all categorical variables to numeric
df[cat_columns] = df[cat_columns].apply ( lambda x: pd.factorize (x)[ 0 ])

y = df['Price']
#Создаем список признаков, на основании которых будем строить модели
features = [
            'Suburb',
            #'Address',
            'Rooms',
            'Type',
            'Method',
            'SellerG',
            'Date',
            'Distance',
            'Postcode',
            #'Bedroom2',
            'Bathroom',
            'Car',
            'Landsize',
            'BuildingArea',
            'YearBuilt',
            'CouncilArea',
            'Lattitude',
            'Longtitude',
            'Regionname',
            'Propertycount',
            'Days'
           ]
for feature in features:
    df[feature].fillna(df[feature].mean(), inplace=True)



print_info_dataset(df)
#print_null_val(df)
#Создаем датафрейм, состоящий из признаков, выбранных ранее
X = df[features]
#Проводим случайное разбиение данных на выборки для обучения (train) и валидации (val), по умолчанию в пропорции 0.75/0.25
train_X, val_X, train_y, val_y = train_test_split(X, y, random_state=1)

rf_model = RandomForestRegressor(n_estimators=100,
                                 n_jobs=-1,
                                 bootstrap=False,
                                 criterion='absolute_error',
                                 max_features=3,
                                 random_state=1,
                                 max_depth=100,
                                 min_samples_split=5
                                 )
#Проводим подгонку модели на обучающей выборке
rf_model.fit(train_X, train_y)

#Вычисляем предсказанные значения цен на основе валидационной выборки
rf_prediction = rf_model.predict(val_X).round(0)

#Вычисляем и печатаем величины ошибок при сравнении известных цен квартир из валидационной выборки с предсказанными моделью
print_metrics(rf_prediction, val_y)

#Рассчитываем важность признаков в модели Random forest
importances = rf_model.feature_importances_
std = np.std([tree.feature_importances_ for tree in rf_model.estimators_],
             axis=0)
indices = np.argsort(importances)[::-1]

#Печатаем рейтинг признаков
print("Рейтинг важности признаков:")
for f in range(X.shape[1]):
    print("%d. %s (%f)" % (f + 1, features[indices[f]], importances[indices[f]]))

#Строим столбчатую диаграмму важности признаков
plt.figure()
plt.title("Важность признаков")
plt.bar(range(X.shape[1]), importances[indices], color="g", yerr=std[indices], align="center")
plt.xticks(range(X.shape[1]), indices)
plt.xlim([-1, X.shape[1]])
plt.show()