import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot
from IPython.core.pylabtools import figsize
from sklearn.model_selection import train_test_split, validation_curve, learning_curve
from sklearn.preprocessing import MinMaxScaler
from sklearn.impute import SimpleImputer
from sklearn.neighbors import KNeighborsRegressor
import warnings

warnings.filterwarnings('ignore')
pyplot.rcParams['font.size'] = 22
sns.set(font_scale=2)


# Вычисление средней абсолютной ошибки
def mae(y_true, y_pred):
    return np.mean(abs(y_true - y_pred))


# Вычисление средней 'относительной' ошибки
def mre(y_true, y_pred):
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


# Обучение и тестирование модели
def fit_and_evaluate(model):
    model.fit(X, y)

    model_pred = model.predict(X_test)
    model_mae_and_mre = (mae(y_test, model_pred), mre(y_test, model_pred))

    return model_mae_and_mre


# Таблица отсутствующих значений в процентном соотношении
def missing_values_table(df):
    mis_val = df.isnull().sum()
    mis_val_percent = 100 * df.isnull().sum() / len(df)
    mis_val_table = pd.concat([mis_val, mis_val_percent], axis=1)
    mis_val_table_ren_columns = mis_val_table.rename(
        columns={0: 'Отсутствующие значения', 1: '% от всех значений'})
    mis_val_table_ren_columns = mis_val_table_ren_columns[
        mis_val_table_ren_columns.iloc[:, 1] != 0].sort_values(
        '% от всех значений', ascending=False).round(1)
    print("В data frame содержится " + str(df.shape[1]) + " столбцов.\n"
                                                          "Всего " + str(
        mis_val_table_ren_columns.shape[0]) +
          " столбец с отсутствующими значениями.")
    return mis_val_table_ren_columns


def plot_learning_curve(estimator, title, X, y, axes=None, ylim=None, cv=None,
                        n_jobs=None, train_sizes=np.linspace(.1, 1.0, 5)):
    """
    Generate 3 plots: the test and training learning curve, the training
    samples vs fit times curve, the fit times vs score curve.

    Parameters
    ----------
    estimator : estimator instance
        An estimator instance implementing `fit` and `predict` methods which
        will be cloned for each validation.

    title : str
        Title for the chart.

    X : array-like of shape (n_samples, n_features)
        Training vector, where ``n_samples`` is the number of samples and
        ``n_features`` is the number of features.

    y : array-like of shape (n_samples) or (n_samples, n_features)
        Target relative to ``X`` for classification or regression;
        None for unsupervised learning.

    axes : array-like of shape (3,), default=None
        Axes to use for plotting the curves.

    ylim : tuple of shape (2,), default=None
        Defines minimum and maximum y-values plotted, e.g. (ymin, ymax).

    cv : int, cross-validation generator or an iterable, default=None
        Determines the cross-validation splitting strategy.
        Possible inputs for cv are:

          - None, to use the default 5-fold cross-validation,
          - integer, to specify the number of folds.
          - :term:`CV splitter`,
          - An iterable yielding (train, test) splits as arrays of indices.

        For integer/None inputs, if ``y`` is binary or multiclass,
        :class:`StratifiedKFold` used. If the estimator is not a classifier
        or if ``y`` is neither binary nor multiclass, :class:`KFold` is used.

        Refer :ref:`User Guide <cross_validation>` for the various
        cross-validators that can be used here.

    n_jobs : int or None, default=None
        Number of jobs to run in parallel.
        ``None`` means 1 unless in a :obj:`joblib.parallel_backend` context.
        ``-1`` means using all processors. See :term:`Glossary <n_jobs>`
        for more details.

    train_sizes : array-like of shape (n_ticks,), dtype={int, float}
        Relative or absolute numbers of training examples that will be used to
        generate the learning curve. If the ``dtype`` is float, it is regarded
        as a fraction of the maximum size of the training set (that is
        determined by the selected validation method), i.e. it has to be within
        (0, 1]. Otherwise it is interpreted as absolute sizes of the training
        sets. Note that for classification the number of samples usually have
        to be big enough to contain at least one sample from each class.
        (default: np.linspace(0.1, 1.0, 5))
    """
    if axes is None:
        _, axes = pyplot.subplots(1, 3, figsize=(20, 5))

    axes[0].set_title(title)
    if ylim is not None:
        axes[0].set_ylim(*ylim)
    axes[0].set_xlabel("Training examples")
    axes[0].set_ylabel("Score")

    train_sizes, train_scores, test_scores, fit_times, _ = \
        learning_curve(estimator, X, y, cv=cv, n_jobs=n_jobs,
                       train_sizes=train_sizes,
                       return_times=True)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    fit_times_mean = np.mean(fit_times, axis=1)
    fit_times_std = np.std(fit_times, axis=1)

    # Plot learning curve
    axes[0].grid()
    axes[0].fill_between(train_sizes, train_scores_mean - train_scores_std,
                         train_scores_mean + train_scores_std, alpha=0.1,
                         color="r")
    axes[0].fill_between(train_sizes, test_scores_mean - test_scores_std,
                         test_scores_mean + test_scores_std, alpha=0.1,
                         color="g")
    axes[0].plot(train_sizes, train_scores_mean, 'o-', color="r",
                 label="Training score")
    axes[0].plot(train_sizes, test_scores_mean, 'o-', color="g",
                 label="Cross-validation score")
    axes[0].legend(loc="best")

    # Plot n_samples vs fit_times
    axes[1].grid()
    axes[1].plot(train_sizes, fit_times_mean, 'o-')
    axes[1].fill_between(train_sizes, fit_times_mean - fit_times_std,
                         fit_times_mean + fit_times_std, alpha=0.1)
    axes[1].set_xlabel("Training examples")
    axes[1].set_ylabel("fit_times")
    axes[1].set_title("Scalability of the model")

    # Plot fit_time vs score
    axes[2].grid()
    axes[2].plot(fit_times_mean, test_scores_mean, 'o-')
    axes[2].fill_between(fit_times_mean, test_scores_mean - test_scores_std,
                         test_scores_mean + test_scores_std, alpha=0.1)
    axes[2].set_xlabel("fit_times")
    axes[2].set_ylabel("Score")
    axes[2].set_title("Performance of the model")

    return pyplot


path_less = "D:\\Лабы по машинному обучению\\Лаба4\\5\\HOUSE_PRICES_LESS.csv"

data_less = pd.read_csv(path_less, delimiter=",")

# Процент пропущенных значений для каждого признака
missing_df = missing_values_table(data_less)
print(missing_df)

missing_columns = list(missing_df[missing_df['% от всех значений'] >= 30].index)
data = data_less.drop(columns=list(missing_columns))
print(f'Столбцы, которые нужно убрать из выборки: {missing_columns}')

# Замена Not Available на not a number
data = data.replace({'Not Available': np.nan})

# Первичный анализ признаков
print(f'Первичный анализ: {data.describe()}')

print(f'Медиана: \n{data.median()}\nМода: \n{data.mode()}')

# Все корреляции с целевым параметром
correlations_data = data.corr()['Price'].sort_values()
print(f'Корреляции с целевым параметром: \n{correlations_data}')

# Первичный визуальный анализ признаков

# График плотности Price, раскрашенный в зависимости от типа
# types = data.dropna(subset=['Price'])
# types = types['Type'].value_counts()
# types = list(types.index)
#
# for t in types:
#     subset = data[data['Type'] == t]
#     sns.kdeplot(subset['Price'].dropna(),
#                 label=t, fill=False, alpha=0.8)
# figsize(12, 10)
# pyplot.xlabel('Цена', size=20)
# pyplot.ylabel('Плотность', size=20)
# pyplot.title('График плотности целевого параметра Price для каждого типа здания', size=28)
# pyplot.legend(loc="best")
# pyplot.show()

# График плотности Price, раскрашенный в зависимости от региона
# rns = data.dropna(subset=['Price'])
# rns = rns['Regionname'].value_counts()
# rns = list(rns.index)
#
# for r in rns:
#     subset = data[data['Regionname'] == r]
#     sns.kdeplot(subset['Price'].dropna(),
#                 label=r, fill=False, alpha=0.8)
#
# figsize(12, 10)
# pyplot.xlabel('Цена', size=20)
# pyplot.ylabel('Плотность', size=20)
# pyplot.title('График плотности целевого параметра Price для каждого региона', size=28)
# pyplot.legend(loc="best")
# pyplot.show()

# Гитограмма для параметра Type
# data['Type'].hist()
# pyplot.show()


# Топ 10 продавцов
# values, bins, patches = pyplot.hist(data['SellerG'], bins=1000)
# order = np.argsort(values)[::-1]
# for i in order[:10]:
#     patches[i].set_color('fuchsia')

# Зависимость стоимости от количества комнат
# pyplot.scatter(data['Price'], data['Rooms'])

# Зависимость стоимости от региона
# pyplot.scatter(data['Regionname'], data['Price'])

# Диаграмма размаха цены здания в зависимости от типа
# data.boxplot(column=['Price'], by=['Type'], grid=False)

# Топ 10 продавцов в виде круговой диаграммы
# data.groupby(['SellerG']).size().sort_values().tail(10).plot(kind='pie', autopct='%1.0f%%')
# pyplot.show()

# Преобразование данных
features = data.copy()
numeric_subset = data.select_dtypes('number')

for col in numeric_subset.columns:
    if col == 'Price':
        next
    else:
        numeric_subset['log_' + col] = np.log(numeric_subset[col])

categorical_subset = data[['Type', 'Regionname']]
categorical_subset = pd.get_dummies(categorical_subset)

features = pd.concat([numeric_subset, categorical_subset], axis=1)

# Разделение на обучающую и тестовую выборки
price = features[features['Price'].notnull()]

features = price.drop(columns='Price')
targets = pd.DataFrame(price['Price'])

features = features.replace({np.inf: np.nan, -np.inf: np.nan})

X, X_test, y, y_test = train_test_split(features, targets, test_size=0.25, random_state=42)

baseline_guess = np.median(y)

print(
    f'Средняя абсолютная ошибка на тестовом наборе = {mae(y_test, baseline_guess).item()}, что в процентном соотношении = '
    f'{mre(y_test, baseline_guess).item()}%')

# Заполнение отсутствующих значений
imputer = SimpleImputer(strategy='median')

imputer.fit(X)

X = imputer.transform(X)
X_test = imputer.transform(X_test)

# Масштабирование значений
scaler = MinMaxScaler(feature_range=(0, 1))

scaler.fit(X)

X = scaler.transform(X)
X_test = scaler.transform(X_test)

y = np.array(y).reshape((-1,))
y_test = np.array(y_test).reshape((-1,))

print('Количество отсутствующих значений в наборе для обучения: ', np.sum(np.isnan(X)))
print('Количество отсутствующих значений в наборе для тестирования:  ', np.sum(np.isnan(X_test)))

# Модель
knn = KNeighborsRegressor(n_neighbors=10)
knn_mae = fit_and_evaluate(knn)

print(f'MAE для модели kNN = {knn_mae[0].item()}, что в процентном соотношении = {knn_mae[1].item()}%')

pyplot.style.use('fivethirtyeight')
figsize(8, 6)

# Прогноз
pred = knn.predict(X_test)
print(f'Прогноз: \n{pred}\nЗначения: \n{y_test}')
sns.kdeplot(pred, label='Прогноз')
sns.kdeplot(y_test, label='Значения')

pyplot.xlabel('Цена')
pyplot.ylabel('Плотность')
pyplot.title('Значения и прогноз')
pyplot.legend(loc="best")
pyplot.show()

print(f'X:\n{X}')
print(f'y:\n{y}')


# train_sizes, train_scores, test_scores = learning_curve(
#      KNeighborsRegressor(n_neighbors=10), X, y)
#
# print(f'Train: {train_scores}')
# print(f'Test: {test_scores}')

fig, axes = pyplot.subplots(3, 2, figsize=(10, 15))

title = "Learning Curves (kNN)"
estimator = KNeighborsRegressor(n_neighbors=10)
plot_learning_curve(estimator, title, X, y)
pyplot.show()


# param_range = np.logspace(-10, 10, 3)
# train_scores, test_scores = validation_curve(KNeighborsRegressor(), X, y,
#                                              param_name="n_neighbors", param_range=param_range, cv=5)
#
# print(f'Train: {train_scores}')
# print(f'Test: {test_scores}')

# train_scores_mean = np.mean(train_scores, axis=1)
# train_scores_std = np.std(train_scores, axis=1)
# test_scores_mean = np.mean(test_scores, axis=1)
# test_scores_std = np.std(test_scores, axis=1)
#
# pyplot.title("Validation Curve with kNN")
# pyplot.xlabel("n_neighbors")
# pyplot.ylabel("Score")
# lw = 2
# pyplot.semilogx(param_range, train_scores_mean, label="Training score",
#              color="darkorange", lw=lw)
# pyplot.fill_between(param_range, train_scores_mean - train_scores_std,
#                  train_scores_mean + train_scores_std, alpha=0.2,
#                  color="darkorange", lw=lw)
# pyplot.semilogx(param_range, test_scores_mean, label="Cross-validation score",
#              color="navy", lw=lw)
# pyplot.fill_between(param_range, test_scores_mean - test_scores_std,
#                  test_scores_mean + test_scores_std, alpha=0.2,
#                  color="navy", lw=lw)
# pyplot.legend(loc="best")
# pyplot.show()
